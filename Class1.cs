﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogPractice
{
    internal class ConsoleLog
    {

        public void Error(string @event,string message) 
        {
            string str;
            str = @event + ":" + message;
            Console.WriteLine(str);
        }
        public void Warn(string @event, string message)
        {
            string str;
            str = @event + ":" + message;
            Console.WriteLine(str);
        }
        public void Info(string @event,string message)
        {
            string str;
            str = @event + ":" + message;
            Console.WriteLine(str);
        }
        public void Debug(string @event,string message)
        {
            string str;
            str = @event + ":" + message;
            Console.WriteLine(str);
        }

    }

    internal class MessageBoxLog
    {
        public void Error(string @event, string message)
        { 
            MessageBox.Show(@event+":"+message);
        }
        public void Warn(string @event, string message)
        {
            MessageBox.Show(@event + ":" + message);
        }
        public void Info(string @event, string message)
        {
            MessageBox.Show(@event + ":" + message);
        }
        public void Debug(string @event, string message)
        {
            MessageBox.Show(@event + ":" + message);
        }
    }
}
