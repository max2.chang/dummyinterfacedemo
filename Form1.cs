﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogPractice
{
    public partial class Form1 : Form
    {
        ConsoleLog _csl = new ConsoleLog();
        MessageBoxLog _mbl = new MessageBoxLog();

        public Form1()
        {
            InitializeComponent();
            cbB_showselect.Items.AddRange(new string[] { "Console", "Message", "Both" });
        }

        private void cbB_showselect_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectitem = cbB_showselect.SelectedItem.ToString();

            switch(selectitem)
            {
                case "Message":
                    _mbl = new MessageBoxLog();
                    break;
                case "Console":
                    _csl = new ConsoleLog();
                    break;
                case "Both":
                    _csl = new ConsoleLog();
                    _mbl = new MessageBoxLog();
                    break;
            }
        }

        private void bt_show_Click(object sender, EventArgs e)
        {
            string eventcontext = tb_showeventlog.Text;
            string messagecontext = tb_showmessagelog.Text;  

            if (cbB_showselect.SelectedItem.ToString() == "Message")
            {
                _mbl.Info(eventcontext, messagecontext);
            }
            else if (cbB_showselect.SelectedItem.ToString() == "Console")
            {
                _csl.Info(eventcontext, messagecontext);
            }
            else
            {
                _mbl.Info(eventcontext, messagecontext);
                _csl.Info(eventcontext, messagecontext);
            }

        }
    }

    

        
}
