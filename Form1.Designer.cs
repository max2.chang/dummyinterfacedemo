﻿namespace LogPractice
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_show = new System.Windows.Forms.Button();
            this.cbB_showselect = new System.Windows.Forms.ComboBox();
            this.tb_showeventlog = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_showmessagelog = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bt_show
            // 
            this.bt_show.Location = new System.Drawing.Point(189, 172);
            this.bt_show.Name = "bt_show";
            this.bt_show.Size = new System.Drawing.Size(89, 23);
            this.bt_show.TabIndex = 0;
            this.bt_show.Text = "SHOW";
            this.bt_show.UseVisualStyleBackColor = true;
            this.bt_show.Click += new System.EventHandler(this.bt_show_Click);
            // 
            // cbB_showselect
            // 
            this.cbB_showselect.FormattingEnabled = true;
            this.cbB_showselect.Location = new System.Drawing.Point(62, 172);
            this.cbB_showselect.Name = "cbB_showselect";
            this.cbB_showselect.Size = new System.Drawing.Size(121, 23);
            this.cbB_showselect.TabIndex = 1;
            this.cbB_showselect.SelectedIndexChanged += new System.EventHandler(this.cbB_showselect_SelectedIndexChanged);
            // 
            // tb_showeventlog
            // 
            this.tb_showeventlog.Location = new System.Drawing.Point(284, 160);
            this.tb_showeventlog.Name = "tb_showeventlog";
            this.tb_showeventlog.Size = new System.Drawing.Size(340, 25);
            this.tb_showeventlog.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(281, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "event";
            // 
            // tb_showmessagelog
            // 
            this.tb_showmessagelog.Location = new System.Drawing.Point(284, 212);
            this.tb_showmessagelog.Name = "tb_showmessagelog";
            this.tb_showmessagelog.Size = new System.Drawing.Size(340, 25);
            this.tb_showmessagelog.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(284, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "message";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_showmessagelog);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_showeventlog);
            this.Controls.Add(this.cbB_showselect);
            this.Controls.Add(this.bt_show);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_show;
        private System.Windows.Forms.ComboBox cbB_showselect;
        private System.Windows.Forms.TextBox tb_showeventlog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_showmessagelog;
        private System.Windows.Forms.Label label2;
    }
}

